import React, { Component } from "react";
import { View, Text } from "react-native";

import ApolloClient from "apollo-boost";
import { ApolloProvider } from "react-apollo";
import { ApolloProvider as ApolloHooksProvider } from "react-apollo-hooks";
import { IntlProvider, addLocaleData, injectIntl } from 'react-intl';
import Navigation from "./src/navigation/navigation";
import DeviceInfo from 'react-native-device-info';
import AsyncStorage from '@react-native-community/async-storage';

import en from 'react-intl/locale-data/en';
import pl from 'react-intl/locale-data/pl';
import translationsEN from './localization/en.json';
import translationsPL from './localization/pl.json';
import { Provider } from "@ant-design/react-native";
addLocaleData([...en, ...pl]);

const translationsData: any = {
  en: translationsEN,
  pl: translationsPL
};

if (process.env.NODE_ENV !== 'production') {
  //const {whyDidYouUpdate} = require('why-did-you-update')
  //whyDidYouUpdate(React)
}

/**
 * to generate apollo components use npm run generate (hooks works as well but first consult docs)
 * - hooks docs: https://github.com/trojanowski/react-apollo-hooks
 * - apollo client docs: https://www.apollographql.com/docs/react/
 *
 * TO RUN THIS APP (very important !!!)
 * - in one window run: react-native start --reset-cache
 * - in another window run: react-native run-android
 * ORDER MATTERS
 * why? because RN 0.59 + is broken for android :)
 */

// TODO: reapir or replace react spring
// TODO: use .env here
// TODO: read documentation about apollo client and how to inject jwt token / cookies into requests
const client = new ApolloClient({
  uri: "http://192.168.100.14:4000/graphql"
  // uri: "http://150.254.68.75:4000/graphql"
});

class App extends Component {
  state = {
    userLanguage: undefined
  };

  componentDidMount() {
    console.log("Setting locale")
    getUserLocale().then(
      settingLang => {
        this.setState({userLanguage: settingLang})
      }
    )
    console.log("Setting locale DONE")
  }

  render() {
    const { userLanguage } = this.state
    if(userLanguage !== undefined) {
      let translations = translationsData[userLanguage];
      console.log("LOCALE: ", userLanguage)
      // return null
      return (
        <ApolloProvider client={client}>
          <ApolloHooksProvider client={client}>
            <Provider>
              <IntlProvider
                locale={this.state.userLanguage}
                messages={translations}
                textComponent={Text}
              >
                <Navigation />
              </IntlProvider>
            </Provider>
          </ApolloHooksProvider>
        </ApolloProvider>
      );
    } else {
      return <Text>Loading translations</Text>
    }
  }
}

const setDefaultUserLocale = async () => {
  try {
    let userLang = DeviceInfo.getPreferredLocales();
    await AsyncStorage.setItem('lang', userLang[0]);
  } catch(e) {
    console.error(e);
  }
}

const getUserLocale = async () => {
  try {
    let userLang = await AsyncStorage.getItem('lang');
    if (!userLang) {
      await setDefaultUserLocale()
      userLang = await AsyncStorage.getItem('lang');
    } else {
      return userLang;
    }
  } catch(e) {
    console.error(e);
  }
}

export default App;