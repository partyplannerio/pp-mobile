import React from 'react';
import * as yup from 'yup';
import { Formik, FastField } from 'formik';
import { Button } from '@ant-design/react-native';
import { View, Text } from 'react-native';
import FormikInput from '../../shared/FormikInput';
import styled from '@emotion/native';
import { LoginMutation, LoginVariables } from '../../../generated/graphql';
//'@generated/graphql';
import { MutationFn, MutationResult } from 'react-apollo';
import { handleLogin } from './AuthService';
import GraphqlError from '../GraphqlError';
import NavigationService from '../../navigation/NavigationService';
import { injectIntl, defineMessages } from 'react-intl';
//'@components/GraphqlError';

interface FormValues {
  email: string;
  password: string;
}

const validationSchema = yup.object().shape<FormValues>({
  email: yup
    .string()
    .email('Please enter a valid email')
    .required('This field is required'),
  password: yup
    .string()
    .required('This field is required')
    .min(6, 'Password has to be at least 6 characters long')
    .max(12, 'Password can have maximum length of 12 characters')
});

const initialValues: FormValues = {
  email: '',
  password: ''
};

const LoginControlsWrapper = styled.View`
  display: flex;
  justify-content: space-between;
`;

const messages = defineMessages({
  email: {
    id: 'login.emailPlaceholder',
    defaultMessage: 'Email',
    description: 'Email field on login screen'
  },
  password: {
    id: 'login.passwordPlaceholder',
    defaultMessage: 'Password',
    description: 'Password field on login screen'
  },
  forgotPassword: {
    id: 'login.forgotPassword',
    defaultMessage: 'Forgot password?',
    description: 'Forgot password link'
  }
})

const LoginForm: React.FC<{
  intl: any;
  mutate: MutationFn<LoginMutation, LoginVariables>;
  mutationResult: MutationResult<LoginMutation>;
}> = ({ intl, mutate, mutationResult: { loading, error } }) => {
  return (
    <Formik
      onSubmit={async formValues => {
        try {
          const response = await mutate({ variables: formValues });
          if (response && response.data) {
            handleLogin(response.data.login.token);
          }
        } catch (e) {
          // error state handled by GraphqlError component
          return null;
        }
      }}
      initialValues={initialValues}
      validationSchema={validationSchema}
    >
      {({ handleSubmit }) => (
        <View>
          <FastField
            aria-label="email-field"
            component={FormikInput}
            type="email"
            name="email"
            placeholder={intl.formatMessage(messages.email)}
            size="large"
          />
          <FastField
            aria-label="password-field"
            component={FormikInput}
            type="password"
            name="password"
            placeholder={intl.formatMessage(messages.password)}
            size="large"
          />
          <Button
            loading={loading}
            size="large"
            data-testid="login-submit"
            onPress={handleSubmit}
          >
            Login
          </Button>
          <Text>{intl.formatMessage(messages.forgotPassword)}</Text>
          <View style={{ marginTop: 24 }}>
            <GraphqlError error={error} />
          </View>
        </View>
      )}
    </Formik>
  );
};

export default injectIntl(LoginForm);
