import React from 'react';
import { Button, Icon } from '@ant-design/react-native';
import styled, { css } from '@emotion/native';
//import SpotifyIcon from '../../../custom-icons/spotify.svg'
//'@customIcons/spotify.svg';
import { FlexBoxFullCenteredStyles } from '../../shared/styles';
// '@shared/styles';
import socialLoginPopup from '../../shared/socialLoginPopup'
//'@shared/socialLoginPopup';
import { handleLogin } from './AuthService';

const getSocialProviderUrl = (provider: 'spotify' | 'facebook' | 'twitter') =>
  `${process.env.BACKEND_URL}/auth/${provider}`;

const ButtonsWrapper = styled.View`
  Button {
    margin-bottom: 10px;
    border: 0;
  }
`;

const SpotifyIconStyles = css`
  svg {
    width: 24px;
    height: 24px;
  }
  color: #1db954;
  @media screen and (max-width: 1050px) {
    color: white;
  }
`;

const SpotifyButtonStyles = css`
  ${FlexBoxFullCenteredStyles}
  &:hover, &:focus {
    color: rgba(0, 0, 0, 0.65);
  }

  @media screen and (max-width: 1050px) {
    background: #1db954;
    color: white;
    &:hover,
    &:focus,
    &:active {
      background: #1db954;
      color: white;
    }
    &:disabled,
    &:disabled:hover {
      background: #1db954;
      .anticon {
        color: rgba(0, 0, 0, 0.25);
      }
    }
  }
`;

export const LoginSocial: React.FC<
  { disabledFromMutation: boolean }
> = ({ disabledFromMutation }) => {
  async function handleSocialLogin(
    provider: 'spotify' | 'twitter' | 'facebook'
  ) {
    try {
      const token = await socialLoginPopup(getSocialProviderUrl(provider));
      handleLogin(token);
    } catch (e) {
      // empty for now, it's being handled by popup
    }
  }

  return (
    <ButtonsWrapper>
      <Button
        disabled={disabledFromMutation}
        type="primary"
        size={'large'}
        onPress={async () => await handleSocialLogin('spotify')}
      >
        Login With Spotify
      </Button>
      <Button
        disabled={disabledFromMutation}
        style={{ backgroundColor: '#4267b2' }}
        type="primary"
        size="large"
        onPress={async () => await handleSocialLogin('facebook')}
      >
        Login with Facebook
      </Button>
      <Button
        disabled={disabledFromMutation}
        type="primary"
        size="large"
        style={{ backgroundColor: '#1da1f2' }}
        onPress={async () => await handleSocialLogin('twitter')}
      >
        Login With Twitter
      </Button>
    </ButtonsWrapper>
  );
};

export default LoginSocial;
