import { ApolloClient } from 'apollo-boost';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../../navigation/NavigationService';

export async function saveToken(token: string) {
  try {
    await AsyncStorage.setItem('token', token);
  } catch (e) {
    console.log(e);
  }
}

export async function getToken(): Promise<any> {
  try {
    return await AsyncStorage.getItem('token');
  } catch (e) {
    console.log(e);
  }
}

export async function handleLogout() {//client: ApolloClient<any>) {
  try {
    NavigationService.navigate('AppLoader')
    await AsyncStorage.setItem('token', '');
    //await client.cache.reset();
    //NavigationService.dismiss();
    //NavigationService.popToTop();
  } catch (e) {
    console.log(e);
  }
}

export function handleLogin(token: string) {
  saveToken(token);
  NavigationService.navigate('Chat');
}
