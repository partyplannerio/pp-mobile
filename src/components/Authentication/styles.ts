import styled from '@emotion/native';
import { FlexBoxFullCenteredStyles } from '../../shared/styles';
import posed from 'react-native-pose';

export const AuthWrapper = styled(
  posed.View({
    enter: { y: 0, opacity: 1 },
    exit: { y: 40, opacity: 0 }
  })
)`

  ${FlexBoxFullCenteredStyles};
`;

export const AuthInnerWrapper = styled.View`
  max-width: 800px;
  width: 500px;
  text-align: left;

  border-left: 2px dashed #d9d9d9;
  padding: 20px 0;
  padding-left: 100px;
`;

export const AuthImageWrapper = styled.View`
  height: 100%;
  flex: 1;
  display: flex;
  ${FlexBoxFullCenteredStyles};
  padding-right: 100px;
`;
