import React from 'react';
import * as yup from 'yup';
import { Formik, FastField, Field } from 'formik';
import { Button } from '@ant-design/react-native';
import { View } from 'react-native';
import FormikInput from '../../../src/shared/FormikInput';
import { SignupComponent } from '../../../generated/graphql';

import { handleLogin } from './AuthService';
import GraphqlError from '../GraphqlError';
import NavigationService from '../../navigation/NavigationService';

interface FormValues {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  confirmPassword: string;
}

const validationSchema = yup.object().shape<FormValues>({
  firstName: yup
    .string()
    .required('This field is required')
    .min(2, 'UserName has to be at least 2 characters long')
    .max(12, 'UserName can have maximum length of 12 characters'),
  lastName: yup
    .string()
    .required('This field is required')
    .min(2, 'UserName has to be at least 2 characters long')
    .max(12, 'UserName can have maximum length of 12 characters'),
  email: yup
    .string()
    .required('This field is required')
    .email('Please enter a valid email'),
  password: yup
    .string()
    .required('This field is required')
    .min(6, 'Password has to be at least 6 characters long')
    .max(12, 'Password can have maximum length of 12 characters'),
  confirmPassword: yup
    .string()
    .oneOf([yup.ref('password'), null], 'Passwords do not match')
    .required('This field is required')
    .min(6, 'Password has to be at least 6 characters long')
    .max(12, 'Password can have maximum length of 12 characters')
});

const initialValues: FormValues = {
  firstName: '',
  lastName: '',
  email: '',
  password: '',
  confirmPassword: ''
};

const RegisterForm: React.FC = () => {
  function handleGoToLoginClick() {
    NavigationService.navigate('Login');
  }

  return (
    <SignupComponent>
      {(mutate, { loading, error }) => (
        <Formik
          onSubmit={async formValues => {
            try {
              const response = await mutate({ variables: formValues });
              if (response && response.data) {
                handleLogin(response.data.signup.token);
              }
            } catch (e) {
              // error state handled by GraphqlError component
              return null;
            }
          }}
          validationSchema={validationSchema}
          initialValues={initialValues}
        >
          { props => (
            <View>
              <FastField
                component={FormikInput}
                name="firstName"
                type="text"
                size="large"
                placeholder="Your First Name"
                aria-label="firstName-field"
              />
              <FastField
                component={FormikInput}
                name="lastName"
                type="text"
                size="large"
                placeholder="Your Last Name"
                aria-label="lastName-field"
              />
              <FastField
                component={FormikInput}
                name="email"
                type="email"
                size="large"
                placeholder="Your Email"
                aria-label="email-field"
              />
              <Field
                component={FormikInput}
                name="password"
                type="password"
                size="large"
                placeholder="Your Password"
                aria-label="password-field"
              />
              <Field
                component={FormikInput}
                name="confirmPassword"
                type="password"
                size="large"
                placeholder="Confirm Password"
                aria-label="confirmPassword-field"
              />
              <Button
                loading={loading}
                style={{ marginBottom: 10 }}
                size="large"
                type="primary"
                onPress={props.handleSubmit}
              >
                Register
              </Button>
              <Button
                disabled={loading}
                onPress={handleGoToLoginClick}
                size="large"
                type="primary"
              >
                Already have account? Login
              </Button>
              <View style={{ marginTop: 24 }}>
                <GraphqlError error={error} />
              </View>
            </View>
          )}
        </Formik>
      )}
    </SignupComponent>
  );
};

export default RegisterForm;
