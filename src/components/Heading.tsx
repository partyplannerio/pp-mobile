import React, { Component } from 'react';
import { 
  Text,
  TextStyle,
  View,
  ViewStyle,
  StyleSheet
} from 'react-native';

const Heading: React.FC = () => {
  return (
    <View style={styles.headingUnderline}>
      <Text style={styles.headingText}>Register</Text>
    </View>
  );
};

interface Style {
  headingUnderline: ViewStyle,
  headingText: TextStyle,
}

const styles = StyleSheet.create<Style>({
  headingUnderline: {
    borderBottomColor: 'blue',
    borderBottomWidth: 2
  },
  headingText: {
    fontWeight: 'bold',
    fontSize: 24
  }
})

export default Heading;