import React from 'react';
import { View, Text } from 'react-native';
import { Modal } from '@ant-design/react-native';
import { ApolloError } from 'apollo-boost';

const GraphqlError: React.FC<{ error?: ApolloError }> = React.memo(
  ({ error }) => {
    let message = null;
    if (error) {
      if (error.graphQLErrors.length > 0) {
        message =
          typeof error.graphQLErrors[0].message === 'string'
            ? error.graphQLErrors[0].message
            : error.graphQLErrors[0].message.message;
      } else {
        message = 'Something went wrong!';
      }
    }
    return message ? <Text>{message}</Text> : null;
  }
);

export default GraphqlError;