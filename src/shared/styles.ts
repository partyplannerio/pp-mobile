import css from '@emotion/css';

export const NotWrappingTextStyles = css`
  max-width: 100%;
  white-space: nowrap;
  overflow: hidden;
`;

export const FlexBoxFullCenteredStyles = css`
  display: flex;
  align-items: center;
  justify-content: center;
`;

export const FlexBoxVerticallyCenteredStyles = css`
  display: flex;
  align-items: center;
`;

export const FlexBoxHorizontallyCenteredStyles = css`
  display: flex;
  justify-content: center;
`;
