import React from "react";
import { FieldProps } from "formik";
import { InputItem } from "@ant-design/react-native";
import { InputItemProps } from "@ant-design/react-native/lib/input-item";

// TODO: read formik documentation
const FormikInput: React.FC<FieldProps & Exclude<InputItemProps, "error">> = ({
  field,
  form,
  ...antdProps
}) => {
  const errorFeedback = form.errors[field.name];
  const isInvalid = form.errors[field.name] && form.touched[field.name];
  const isValid = !form.errors[field.name] && form.touched[field.name];

  function handleBlur() {
    form.setFieldTouched(field.name);
    field.onBlur(field.name)
  }

  // TODO: implement some kind of error feedback (additional visual feedback like this field is required or such)
  return (
    <InputItem
      value={field.value}
      onChange={field.onChange(field.name)}
      onBlur={handleBlur}
      error={isInvalid as boolean}
      {...antdProps}
    />
  );


};


export default FormikInput;
