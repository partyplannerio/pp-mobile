import { NavigationActions, NavigationParams, StackActions } from 'react-navigation';

export default class NavigationService {

  private static navigator: any;

  static setTopLevelNavigator = (navigatorRef: any):void => {
    NavigationService.navigator = navigatorRef;
  }

  static navigate = (routeName: string, params: NavigationParams = {}):void => {
    NavigationService.navigator.dispatch(
      NavigationActions.navigate({
        routeName,
        params,
      })
    )
  }

  static pop = (howManyScreensToPop: number = 1): void => {
    NavigationService.navigator.dispatch(
      StackActions.pop({
        n: howManyScreensToPop
      })
    )
  }

  static popToTop = (): void => {
    NavigationService.navigator.dispatch(StackActions.popToTop({}))
  }

}

