import React from 'react';
import { createStackNavigator, createSwitchNavigator, createBottomTabNavigator,  createAppContainer } from 'react-navigation';

import NavigationService from './NavigationService';

import Register from '../screens/Register';
import Login from '../screens/Login';
import Welcome from '../screens/Welcome';
import SocialLogin from '../screens/SocialLogin';
import Dashboard from '../screens/Dashboard';
import Parties from '../screens/Parties';
import Chat from '../screens/Chat';
import Calendar from '../screens/Calendar';
import Profile from '../screens/Profile';
import AppLoader from '../screens/AppLoader';
import { Icon } from '@ant-design/react-native';
import { injectIntl, intlShape, defineMessages } from 'react-intl';

// TODO: Translate text inside bottom bar navigation
const AppStack = createBottomTabNavigator({
  Parties: {
    screen: Parties
  },
  Chat: {
    screen: Chat
  },
  Calendar: {
    screen: Calendar
  },
  Profile: {
    screen: Profile
  }
},
{
defaultNavigationOptions: ({ navigation }) => ({
  tabBarIcon: ({ focused, horizontal, tintColor }) => {
    const { routeName } = navigation.state;
    let enteredTintColor: any = tintColor;
    let IconComponent = Icon;
    let iconName: any;
    if (routeName === 'Calendar') {
      iconName = `calendar`;
      // Sometimes we want to add badges to some icons. 
      // You can check the implementation below.
      //IconComponent = HomeIconWithBadge; 
    } else if (routeName === 'Profile') {
      iconName = `user`;
    } else if (routeName === 'Chat') {
      iconName = `message`
    } else if (routeName === 'Parties') {
      iconName = `notification`
    }

    // You can return any component that you like here!
    return <IconComponent name={iconName} size="md" color={enteredTintColor} />;
  },
}),
tabBarOptions: {
  activeTintColor: 'blue',
  inactiveTintColor: 'gray',
},
}

)


const AuthStack = createStackNavigator({
  Register: {
    screen: Register
  },
  Login: {
    screen: Login
  },
  Welcome: {
    screen: Welcome
  },
  SocialLogin: {
    screen: SocialLogin
  }
},{initialRouteName: "Welcome"});

const AppNavigator = createSwitchNavigator({
  AppLoader: {
    screen: AppLoader
  },
  App: AppStack,
  Auth: AuthStack,
},{initialRouteName: "AppLoader"})

const AppContainer = createAppContainer(AppNavigator);

class Navigation extends React.Component {
  render() {
    return  (
      <AppContainer
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef)
        }}
      />
    );
  }
}

export default injectIntl(Navigation);