import React from 'react';
import { Text } from 'react-native';
import { ApolloClient } from 'apollo-boost';
import { MeQueryQuery, MeQueryDocument, MeQueryComponent } from '../../generated/graphql';
import { handleLogout } from '../components/Authentication/AuthService';

export function HOC<C>(WrappedComponent: React.ComponentType<C>) {
  return class Comp extends React.Component {
 

    render() {
   
      return(
        <MeQueryComponent>
          {({data, loading ,error}) => {
            if (loading) return null;
            if (data) return <WrappedComponent {...this.props as C}/>
            if (error) return null;
          }}
        </MeQueryComponent>
      ) 
    }
  }
}


