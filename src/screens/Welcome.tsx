import React from 'react';
import { Text } from 'react-native';
import styled from '@emotion/native';
import { Button } from '@ant-design/react-native';
import NavigationService from '../navigation/NavigationService';
import { FormattedMessage } from 'react-intl';

const HomeHeroImage = styled.View`
  width: 100%;
  position: relative;
  flex: 1;
`;

const HomeHeroControlsWrapper = styled.View`
  margin-top: 25px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

const Home: React.FC = (props) => {
  return (
    <HomeHeroImage>
        <FormattedMessage id="welcome.slogan1" defaultMessage="Simplify your party life" description="Welcome page slogan one" />
        <FormattedMessage id="welcome.slogan2" defaultMessage="Organize, make plans, schedule, invite others!" description="Welcome page slogan one" />
        <HomeHeroControlsWrapper>
          <Button onPress={() => NavigationService.navigate('Register')}>
            <FormattedMessage
              id="welcome.register"
              defaultMessage="Register Now"
              description="Welcome screen register"
            />
          </Button>
          <Button onPress={() => NavigationService.navigate('Login')}>
            <FormattedMessage
              id="welcome.login"
              defaultMessage="Login Now"
              description="Welcome screen login"
            />
          </Button>
        </HomeHeroControlsWrapper>
    </HomeHeroImage>
  );
};

export default Home;