import React from 'react';

import {
  AuthWrapper,
  AuthInnerWrapper
} from '../components/Authentication/styles'; 
import LoginForm from '../components/Authentication/LoginForm';
import LoginSocial from '../components/Authentication/LoginSocial';
import { LoginComponent } from '../../generated/graphql';
import { PoseGroup } from 'react-pose';
import { Text, View } from 'react-native';

const Login: React.FC = () => {
  return (
    <LoginComponent>
      {(mutate, mutationResult) => (
        <View>
          <LoginForm mutate={mutate} mutationResult={mutationResult} />
          <LoginSocial disabledFromMutation={mutationResult.loading} />
        </View>
      )}
    </LoginComponent>
  );
};

export default Login;