import * as React from 'react';
import { View, Text } from 'react-native';
import { Button, Picker, List } from '@ant-design/react-native';
import NavigationService from '../navigation/NavigationService';
import { handleLogout } from '../components/Authentication/AuthService';
import AsyncStorage from '@react-native-community/async-storage';
import { FormattedMessage, defineMessages, injectIntl} from 'react-intl';
import { PickerData } from '@ant-design/react-native/lib/picker/PropsType';


const Profile: React.FC<{}> = ({intl}) => {
  
  const availableLanguages: any = [
    {value: "en", label: "English"},
    {value: "pl", label: "Polish"}
  ]

  const [state, setState] = React.useState<string[]>([])

  React.useEffect(() => {
    async function getCurrentLanguage() {
      const lang = await getCurrentLanguageFromStorage();
      if (lang == null || lang.trim().length <=0) return;
      console.log('fetced land', lang)
      setState([lang])
    }
    getCurrentLanguage()
  }, [])

  const getCurrentLanguageFromStorage = async () => {
    try {
      return await AsyncStorage.getItem('lang');
    } catch (e) {
      console.error(e);
    }
  }

  React.useEffect(() => {
    if (state.length == 0) return;
    const [lang] = state;
    async function changeLanguage() {
      await changeLanguageInStorage(lang)
    }
    changeLanguage()
  }, [state])

  async function changeLanguageInStorage(lang:string) {
    try {
      await AsyncStorage.setItem('lang', lang);
    } catch(e) {
      console.error(e)
    }
  }

  const messages = defineMessages({
    ok: {
      id: 'options.ok',
      defaultMessage: 'Ok',
      description: 'Accept/approve option'
    },
    dismiss: {
      id: 'options.dismiss',
      defaultMessage: 'Dismiss',
      description: 'Disapprove option',
    },
    logout: {
      id: 'options.logout',
      defaultMessage: 'Log Out',
      description: 'Logout option'
    }
  })
  
  return (
    <View>
      <List>
        <Picker
          data={availableLanguages}
          cols={1}
          value={state}
          onChange={(e) => setState(e as string[]) }
          okText={intl.formatMessage(messages.ok)}
          dismissText={intl.formatMessage(messages.dismiss)}
        >
          <List.Item>
            <FormattedMessage
              id="profileScreen.changeLanguage"
              defaultMessage="Change your language"
              description="Change user labguage on profile screen"
            />
          </List.Item>
        </Picker>
      </List>
      <Button
        onPress={() => handleLogout()}
      >
        {intl.formatMessage(messages.logout)}
      </Button>
    </View>
  ) 
}

export default injectIntl(Profile);
