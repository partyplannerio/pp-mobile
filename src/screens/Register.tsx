import React from 'react';
import RegisterForm from '../components/Authentication/RegisterForm';

const Register: React.FC = () => {
  return <RegisterForm/>
};

export default Register;