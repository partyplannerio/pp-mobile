import * as React from 'react';
import { View, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import NavigationService from '../navigation/NavigationService';

const AppLoader: React.FC<{}> = () => {
  isUserLogged();
  return (
    <View>
      <Text>PartyPlanner</Text>
    </View>
  )
}

const isUserLogged = async () => {
  try {
    const token = await AsyncStorage.getItem('token');
    console.log(token);
    NavigationService.navigate(token ? 'App' : 'Auth');
  } catch(e) {
    console.error(e);
  }
};

export default AppLoader;