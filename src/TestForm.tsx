import React from "react";
import * as yup from "yup";
import { Formik, Field } from "formik";
import { View } from "react-native";
import FormikInput from "./shared/FormikInput";

import { Button } from "@ant-design/react-native";

interface TestFormValues {
  firstName: string;
  lastName: string;
}

const initialFormValues: TestFormValues = {
  firstName: "",
  lastName: ""
};

const validationSchema = yup.object().shape<TestFormValues>({
  firstName: yup.string().required(),
  lastName: yup.string().required()
});

//TODO:
/*
 *  Read about the difference with field vs fast field
 *  Read about "connect" method exposed by formik
 */

const TestForm: React.FC = () => {
  return (
    <Formik
      validationSchema={validationSchema}
      initialValues={initialFormValues}
      onSubmit={() => console.log("works")}
    >
      {({ handleSubmit }) => (
        <View>
          <Field
            component={FormikInput}
            placeholder="First Name"
            name="firstName"
          />
          <Button onPress={handleSubmit}>Submit me</Button>
        </View>
      )}
    </Formik>
  );
};

export default TestForm;
