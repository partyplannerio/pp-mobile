import { gql } from "apollo-boost";

const ME_QUERY = gql`
  query MeQuery {
    me {
      firstName
      lastName
      id
    }
  }
`;
